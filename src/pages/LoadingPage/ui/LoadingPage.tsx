const LoadingPage = () => {
  return (
    <div
      style={{
        width: '100%',
        height: 'calc(100vh - var(--h-navbar)',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <h1 style={{}}>LoadingPage...</h1>
    </div>
  )
}

export default LoadingPage
