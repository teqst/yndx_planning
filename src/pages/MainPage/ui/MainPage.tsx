import React, {
  ChangeEvent,
  FormEventHandler,
  useEffect,
  useMemo,
  useState
} from 'react'
import cls from './MainPage.module.scss'
import TaskCard from '@/widgets/TaskCard/ui/TaskCard'
import { classNames } from '@/shared/lib/classNames/classNames'
import { UserTasksCard } from '@/widgets/UserTasksCard'
import { SideMenu } from '@/widgets/SideMenu'
import { NavigationBar } from '@/widgets/NavigationBar'

function MainPage() {
  const [usersTasks, setUsersTasks] = useState([])
  const [tasks, setTasks] = useState([])

  const tasksByUsers: Record<string, any[]> = useMemo(() => {
    return tasks.reduce((acc: any, cv: any) => {
      if (!acc[cv?.taskResolver]) {
        acc[cv?.taskResolver] = [cv]
      } else {
        acc[cv?.taskResolver] = [...acc[cv?.taskResolver], cv]
      }

      return acc
    }, {})
  }, [tasks])

  function onChangeSprint(e: ChangeEvent<HTMLSelectElement>) {
    const sprintId = e.target.value
    console.log(sprintId)
  }

  async function getUsersStatistics() {
    const res = await fetch('./UserStatistics.json')
    const users = await res.json()
    setUsersTasks(users)
    console.log('GET_USER_TASKS')
  }

  async function getTasks() {
    const res = await fetch('./Tasks.json')
    const tasks = await res.json()
    setTasks(tasks)
    console.log('GET_TASKS')
  }

  function getSprints() {}

  function getSprint(id: string) {}

  useEffect(() => {
    getUsersStatistics()
    getTasks()
    console.log(tasksByUsers)
  }, [])

  return (
    <>
      <NavigationBar className={''} sprintChange={onChangeSprint} />
      <div className={classNames(cls.MainPage, {}, [])}>
        <SideMenu className={cls.MainPageAside}>
          {usersTasks.map((userTasks: any, idx: number) => (
            <UserTasksCard
              key={idx}
              options={userTasks.options}
              userName={userTasks.username}
            />
          ))}
        </SideMenu>
        <div className={cls.MainPageContent}>
          {Object.entries(tasksByUsers).map(([key, value]) => (
            <div className={cls.MainPageUserGroups} key={key}>
              <h3 className={cls.MainPageUserGroupsTitle}>{key}</h3>
              <div className={cls.MainPageUserGroupsList}>
                {value.map((task: any, idx: number) => (
                  <TaskCard
                    key={idx}
                    taskResolver={task.taskResolver}
                    taskTime={'2 hrs'}
                    taskLink={task.taskLink}
                    taskName={task.taskName}
                    taskStatus={task.taskStatus}
                    taskDescription={task.taskDescription}
                  />
                ))}
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}

export default MainPage
