import { classNames } from '@/shared/lib/classNames/classNames'
import cls from './AppButton.module.scss'
import { ButtonHTMLAttributes, FC, ReactPropTypes } from 'react'
import App from '@/app/App'

export enum AppButtonTypes {
  Button = 'button',
  Link = 'link'
}

interface AppButtonProps
  extends ButtonHTMLAttributes<HTMLButtonElement | HTMLAnchorElement> {
  className?: string
  btntype?: AppButtonTypes
  href?: string
}

const AppButton: FC<AppButtonProps> = (props) => {
  const { className, children, btntype = AppButtonTypes.Button } = props

  return btntype === AppButtonTypes.Link ? (
    <a {...props} className={classNames(cls?.AppButtonLink, {}, [className])}>
      {children}
    </a>
  ) : (
    <button {...props} className={classNames(cls?.AppButton, {}, [className])}>
      {children}
    </button>
  )
}

export default AppButton
