import { classNames } from '@/shared/lib/classNames/classNames'
import cls from './AppLogo.module.scss'

interface AppLogoProps {
  className?: string
}

const AppLogo = ({ className }: AppLogoProps) => {
  return (
    <div className={classNames(cls?.AppLogo, {}, [className])}>
      <h2 className={cls.static}>hello_</h2>
      <div className={cls.dynamicContainer}>
        <h2>planning</h2>
        <h2 style={{ color: 'Crimson' }}>× pain ×</h2>
        <h2 style={{ color: 'Chartreuse' }}>obsession</h2>
        <h2 style={{ color: 'var(--light)' }}>
          <span style={{ color: 'red' }}>y</span>
          andex
        </h2>
      </div>
    </div>
  )
}

export default AppLogo
