import { classNames } from '@/shared/lib/classNames/classNames'
import cls from './AppBadge.module.scss'
import { FC } from 'react'

export enum AppBadgeStates {
  DEFAULT = 'default',
  SUCCESS = 'success',
  WARNING = 'warning',
  ERROR = 'error',
  INFO = 'info'
}

interface AppBadgeProps {
  className?: string
  state?: AppBadgeStates
}

const AppBadge: FC<AppBadgeProps> = ({
  className,
  children,
  state = AppBadgeStates.DEFAULT
}) => {
  return (
    <div className={classNames(cls?.AppBadge, {}, [className, cls[state]])}>
      {children}
    </div>
  )
}

export default AppBadge
