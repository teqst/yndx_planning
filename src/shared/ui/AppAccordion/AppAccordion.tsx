import { classNames } from '@/shared/lib/classNames/classNames'
import cls from './AppAccordion.module.scss'
import { FC } from 'react'
import Arrow from '@/shared/assets/arrowTop.svg'

interface AppAccordionProps {
  className?: string
  children?: any
}

const AppAccordion = ({ className, children }: AppAccordionProps) => {
  return (
    <details className={classNames(cls.AppAccordion, {}, [className])}>
      {children}
    </details>
  )
}

interface AppAccordionHeaderProps {
  className?: string
  summary?: string
}
const AppAccordionHeader: FC<AppAccordionHeaderProps> = ({
  children,
  className,
  summary,
  ...props
}) => {
  return (
    <summary
      {...props}
      className={classNames(cls?.AppAccordionHeader, {}, [className])}
    >
      {summary ? summary : children}
      <Arrow className={cls.AppAccordionHeaderArrow} />
    </summary>
  )
}

AppAccordion.Header = AppAccordionHeader

interface AppAccordionContentProps {
  className?: string
}
const AppAccordionContent: FC<AppAccordionContentProps> = ({
  children,
  className,
  ...props
}) => {
  return (
    <div
      {...props}
      className={classNames(cls?.AppAccordionContent, {}, [className])}
    >
      {children}
    </div>
  )
}

AppAccordion.Content = AppAccordionContent

export default AppAccordion
