import { classNames } from '@/shared/lib/classNames/classNames'
import cls from './AppSelect.module.scss'
import { FC, SelectHTMLAttributes } from 'react'
import ArrowTop from '@/shared/assets/arrowTop.svg'

interface SelectOption {
  id?: number
  title: string
  value: string | number
}

interface AppSelectProps extends SelectHTMLAttributes<HTMLSelectElement> {
  className?: string
  options: SelectOption[]
  placeholder?: string
}

const AppSelect: FC<AppSelectProps> = (props) => {
  const { className, options, placeholder, ...otherProps } = props

  return (
    <div className={classNames(cls.AppSelectContainer, {}, [className])}>
      <select
        {...otherProps}
        className={classNames(cls.AppSelect, {}, [className])}
      >
        <option className={cls.AppSelectOption} value={0}>
          {placeholder}
        </option>
        {options.map((option: SelectOption) => (
          <option
            className={cls.AppSelectOption}
            value={option.value}
            key={option.id}
          >
            {option.title}
          </option>
        ))}
      </select>
      <ArrowTop className={cls.AppSelectArrow} />
    </div>
  )
}

export default AppSelect
