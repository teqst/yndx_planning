import { classNames } from '@/shared/lib/classNames/classNames'
import cls from './AppCheckbox.module.scss'

interface AppCheckboxProps {
  className?: string
  label?: string
  checkBoxSize?: {
    w: string
    h: string
  }
}

const AppCheckbox = (props: AppCheckboxProps) => {
  const { className, label, checkBoxSize = { w: '15px', h: '15px' } } = props

  return (
    <label className={classNames(cls.AppCheckbox, {}, [className, cls.Option])}>
      <input type='checkbox' className={cls.AppCheckboxInput} />
      <span
        className={cls.AppCheckboxBox}
        style={{ width: checkBoxSize.w, height: checkBoxSize.h }}
      />
      {label ? <span className={cls.AppCheckboxLabel}>{label}</span> : ''}
    </label>
  )
}

export default AppCheckbox
