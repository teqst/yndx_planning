import React, { ChangeEvent, FormEventHandler } from 'react'
import { classNames } from '@/shared/lib/classNames/classNames'
import cls from './NavigationBar.module.scss'
import AppSelect from '@/shared/ui/AppSelect/AppSelect'
import AppThemeSwitcher from '@/shared/ui/AppThemeSwitcher/AppThemeSwitcher'
import AppLogo from '@/shared/ui/AppLogo/AppLogo'
import AppButton from '@/shared/ui/AppButton/AppButton'

interface NavbarProps {
  className?: string
  sprintChange: (e: ChangeEvent<HTMLSelectElement>) => void
}

const NavigationBar = ({ className, sprintChange }: NavbarProps) => {
  const options = [
    {
      id: 1,
      title: 'Спринт 1',
      value: 1
    },
    {
      id: 2,
      title: 'Спринт 2',
      value: 2
    }
  ]

  function handlePlanSprint(e: any) {
    console.log(e)
  }

  return (
    <div className={classNames(cls?.navbar, {}, [className])}>
      <AppLogo className={classNames(cls.navbarLogo)} />
      <div className={cls.navbarRight}>
        <AppButton className={cls.navbarSprintBtn} onClick={handlePlanSprint}>
          Запланировать спринт
        </AppButton>
        <AppSelect
          options={options}
          placeholder='Выбрать спринт'
          className={cls.navbarSelect}
          onChange={sprintChange}
        />
        <AppThemeSwitcher />
      </div>
    </div>
  )
}

export default NavigationBar
