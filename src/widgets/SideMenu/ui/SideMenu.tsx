import { classNames } from '@/shared/lib/classNames/classNames'
import cls from './SideMenu.module.scss'
import React, { FC } from 'react'

interface SideMenuProps {
  className?: string
}

const SideMenu: FC<SideMenuProps> = ({ className, children }) => {
  return (
    <div className={classNames(cls?.SideMenu, {}, [className])}>{children}</div>
  )
}

export default SideMenu
