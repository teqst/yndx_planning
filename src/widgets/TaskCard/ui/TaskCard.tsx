import { classNames } from '@/shared/lib/classNames/classNames'
import cls from './TaskCard.module.scss'
import AppAccordion from '@/shared/ui/AppAccordion/AppAccordion'
import React from 'react'
import AppBadge, { AppBadgeStates } from '@/shared/ui/AppBadge/AppBadge'
import AppButton, { AppButtonTypes } from '@/shared/ui/AppButton/AppButton'
import Time from '@/shared/assets/time.svg'
import AppCheckbox from '@/shared/ui/AppCheckbox/AppCheckbox'

interface TaskCardProps {
  className?: string
  taskName: string
  taskStatus: AppBadgeStates
  taskDescription: string
  taskLink: string
  taskTime: string
  taskResolver?: string
}

const TaskCard = ({
  className,
  taskName,
  taskStatus,
  taskTime,
  taskDescription,
  taskLink,
  taskResolver = ''
}: TaskCardProps) => {
  return (
    <AppAccordion className={classNames(cls?.TaskCard, {}, [className])}>
      <AppAccordion.Header className={cls.TaskCardHeader}>
        <div className={cls.TaskCardHeaderLeft}>
          <AppButton
            btntype={AppButtonTypes.Link}
            className={cls.TaskCardName}
            href={taskLink}
          >
            {taskName}
          </AppButton>
          <AppBadge className={cls.TaskCardStatus} state={taskStatus}>
            {taskStatus}
          </AppBadge>
        </div>
        <div className={cls.TaskCardHeaderRight}>
          <div className={cls.TaskCardTime}>
            <Time className={cls.TaskCardTimeIcon} />
            <span className={cls.TaskCardTimeText}>{taskTime}</span>
          </div>
          <AppCheckbox checkBoxSize={{ w: '40px', h: '40px' }} />
        </div>
      </AppAccordion.Header>
      <AppAccordion.Content className={cls.TaskCardContent}>
        {taskDescription}
      </AppAccordion.Content>
    </AppAccordion>
  )
}

export default TaskCard
