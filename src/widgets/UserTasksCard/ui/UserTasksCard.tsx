import { classNames } from '@/shared/lib/classNames/classNames'
import cls from './UserTasksCard.module.scss'
import React from 'react'
import AppBadge, { AppBadgeStates } from '@/shared/ui/AppBadge/AppBadge'

interface UserTasksCardOptions {
  title: string
  time: string
}

interface UserTasksCardProps {
  className?: string
  userName: string
  options: UserTasksCardOptions[]
}

const UserTasksCard = ({
  className,
  userName,
  options
}: UserTasksCardProps) => {
  return (
    <div className={classNames(cls?.UserTasksCard, {}, [className])}>
      <h4 className={cls.UserTasksCardTitle}>{userName}</h4>
      <div className={cls.UserTasksCardList}>
        {options.map((option: UserTasksCardOptions, idx: number) => (
          <div className={cls.UserTasksCardListItem} key={idx}>
            <span className={cls.UserTasksCardListItemText}>
              {option.title}
            </span>
            <AppBadge state={AppBadgeStates.INFO}>{option.time}</AppBadge>
          </div>
        ))}
      </div>
    </div>
  )
}

export default UserTasksCard
