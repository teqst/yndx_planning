import HtmlWebpackPlugin from "html-webpack-plugin";
import webpack from "webpack";
import { BuildOptions } from "./types/config";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
const progressPlugin = webpack.ProgressPlugin;
import CopyPlugin from "copy-webpack-plugin";
import path from "path";

export default function buildPlugins({ paths }: BuildOptions) {
    return [
        new HtmlWebpackPlugin({
            template: paths.html // Указываем наш index.html, куда монтируется аппа
        }),
        new progressPlugin((percentage: number, message: string, ...args: string[]) => {
            const normalizedPercentage = Math.trunc(percentage * 100)
            console.info('[PROGRESS]:', normalizedPercentage, message, ...args);
        }),
        new MiniCssExtractPlugin({
            filename: 'css/[name][contenthash:8].css',
            chunkFilename: 'css/[name][contenthash:8].css',
        }), // Отделение css из js чанка,
        new CopyPlugin({
            patterns: [
                { from: paths.mock, to: paths.build}
            ]
        })
    ]
}
