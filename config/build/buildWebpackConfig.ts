import webpack from "webpack";
import {BuildOptions} from "./types/config";
import buildPlugins from "./buildPlugins";
import buildResolvers from "./buildResolvers";
import {buildLoaders} from "./buildLoaders";
import {buildDevServer} from "./buildDevServer";

export function buildWebpackConfig(options: BuildOptions): webpack.Configuration {
    const { paths, mode, isDev } = options

    return {
        mode: mode,
        entry: paths.entry,
        output: {
            filename: '[name].[contenthash].js',
            path: paths.build,
            clean: true // Удаляем из билд папки старые файлы
        },
        plugins: buildPlugins(options),
        module: {
            rules: buildLoaders(options)
        },
        resolve: buildResolvers(options),
        devtool: isDev ? 'inline-source-map' : undefined, // sourcemap, чтобы stack trace был более информативный
        devServer: isDev ? buildDevServer(options) : undefined
    }
}
