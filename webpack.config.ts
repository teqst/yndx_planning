import path from 'path'
import webpack from 'webpack'
import 'webpack-dev-server'
import { BuildEnv, BuildMode, BuildPaths } from './config/build/types/config'
import { buildWebpackConfig } from './config/build/buildWebpackConfig'

export default (env: BuildEnv) => {
  const paths: BuildPaths = {
    entry: path.resolve(__dirname, 'src', 'index.tsx'),
    build: path.resolve(__dirname, 'build'),
    html: path.resolve(__dirname, 'public', 'index.html'),
    src: path.resolve(__dirname, 'src'),
    mock: path.resolve(__dirname, 'src', 'shared', 'mock')
  }

  const mode: BuildMode = (env.mode as BuildMode) || 'development'
  const isDev = mode === 'development'
  const PORT = Number(env.port) || 3000

  console.log('MODE: ', mode)
  console.log('PORT: ', PORT)

  const config: webpack.Configuration = buildWebpackConfig({
    mode: mode,
    paths,
    isDev,
    port: PORT
  })

  console.log(paths)
  return config
}
