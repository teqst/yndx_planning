## Стек технологий:

- Самописная сборка на `Webpack 5.82.1`
  - Development сервер с HMR
  - Несколько режимов сборки: Dev & Production
  - Минификация кода
  - Code Splitting
  - Создание sourcemap
  - Работа с препроцессором стилей
  - Работа с `CSS Modules`
- В качестве архитектурной методологии [FSD](https://feature-sliced.design/ru/) - Feature Sliced Design
- Типизация с `Typescript 5.0.4`
- Frontend фреймворк `React 17.0.2`
- Роутинг `React Router `
- Препроцессор `SCSS`
- `Prettier` для форматирования кода

## Архитектура:

Проект на FSD состоит из слоев `(layers)`, каждый слой состоит из слайсов `(slices)` и каждый слайс состоит из сегментов `(segments)`.

<img src="readme_resources/img.png" alt="FSD Architecture" width="500"/>

Слои стандартизированы во всех проектах и расположены вертикально. Модули на одном слое могут взаимодействовать лишь с модулями, находящимися на слоях строго ниже. На данный момент слоев шесть (снизу вверх):

- `shared` — переиспользуемый код, не имеющий отношения к специфике приложения/бизнеса.(например, UIKit, libs, API)
- `entities` (сущности) — бизнес-сущности.(например, User, Product, Order)
- `features` (фичи) — взаимодействия с пользователем, действия, которые несут бизнес-ценность для пользователя.(например, SendComment, AddToCart, UsersSearch)
- `widgets` (виджеты) — композиционный слой для соединения сущностей и фич в самостоятельные блоки(например, IssuesList, UserProfile).
- `pages` (страницы) — композиционный слой для сборки полноценных страниц из сущностей, фич и виджетов.
- `app` — настройки, стили и провайдеры для всего приложения.
